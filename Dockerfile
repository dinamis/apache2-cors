FROM httpd:2.4
RUN sed -i 's#<Directory /var/www/>#<Directory /var/www/>\n        Header set Access-Control-Allow-Origin "*"#g' /usr/local/apache2/conf/httpd.conf
RUN sed -i 's#Listen 80#Listen 8080#g' /usr/local/apache2/conf/httpd.conf
RUN chgrp -R 0 /usr/local/apache2/logs && \
 chmod -R g+rwX /usr/local/apache2/logs
USER 1001
